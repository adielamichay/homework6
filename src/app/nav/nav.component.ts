import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toSignup(){
    this.router.navigate(['/signup']);
  }
  toWelcom(){
    this.router.navigate(['/welcome']);
  }

  constructor(private router:Router) { }

  ngOnInit() {
  }

}
